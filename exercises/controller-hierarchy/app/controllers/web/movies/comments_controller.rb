# frozen_string_literal: true

class Web::Movies::CommentsController < Web::Movies::ApplicationController
  def index
    @comments = resource_movie.comments
  end

  def new
    @comment = resource_movie.comments.build
  end

  def create
    @comment = resource_movie.comments.build(comment_params)

    if @comment.save
      redirect_to movie_comments_path resource_movie
    else
      render :new
    end
  end

  def edit
    @comment = comment
  end

  def update
    @comment = comment

    if @comment.update(comment_params)
      redirect_to movie_comments_path resource_movie
    else
      render :edit
    end
  end

  def destroy
    @comment = comment

    @comment.destroy
    redirect_to movie_comments_path resource_movie
  end

  private

  def comment
    @comment ||= Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:body)
  end
end
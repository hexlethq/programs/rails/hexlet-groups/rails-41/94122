# frozen_string_literal: true

class Web::Movies::ReviewsController < Web::Movies::ApplicationController
  def index
    @reviews = resource_movie.reviews
  end

  def new
    @review = resource_movie.reviews.build
  end

  def create
    @review = resource_movie.reviews.build(review_params)

    if @review.save
      redirect_to movie_reviews_path resource_movie
    else
      render :new
    end
  end

  def edit
    @review = review
  end

  def update
    @review = review

    if @review.update(review_params)
      redirect_to movie_reviews_path resource_movie
    else
      render :edit
    end
  end

  def destroy
    @review = review

    @review.destroy
    redirect_to movie_reviews_path resource_movie
  end

  private

  def review
    @review ||= Review.find(params[:id])
  end

  def review_params
    params.require(:review).permit(:body)
  end
end
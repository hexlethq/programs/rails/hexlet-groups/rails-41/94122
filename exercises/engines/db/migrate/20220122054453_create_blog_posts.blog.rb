# This migration comes from blog (originally 20220122052644)
class CreateBlogPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :blog_posts do |t|
      t.string :title

      t.timestamps
    end
  end
end

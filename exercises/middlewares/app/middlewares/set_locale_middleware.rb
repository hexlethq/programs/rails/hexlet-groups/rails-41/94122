# frozen_string_literal: true

class SetLocaleMiddleware
  # BEGIN
  attr_reader :app

  def initialize(app)
    @app = app
  end

  def call(env)
    I18n.locale = get_locale(env)
    @status, @headers, @response = app.call(env)
    @headers['Content-Language'] = I18n.locale.to_s
    [@status, @headers, @response]
  end

  private

  def get_locale(env)
    if env['HTTP_ACCEPT_LANGUAGE'].nil?
      default_locale = I18n.default_locale
      Rails.logger.debug("The default locale is used - #{default_locale}")
      default_locale
    else
      locale = env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
      Rails.logger.debug("Detected locale - #{locale}")
      locale
    end
  end
  # END
end

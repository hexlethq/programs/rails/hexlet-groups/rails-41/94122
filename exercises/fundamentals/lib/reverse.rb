# frozen_string_literal: true

# BEGIN
def reverse(string)
  reversed_string = ''

  string.each_char do |char|
    reversed_string = "#{char}#{reversed_string}"
  end

  reversed_string
end
# END

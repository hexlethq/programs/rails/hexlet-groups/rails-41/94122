# frozen_string_literal: true

# BEGIN
def convert_number(number)
  if (number % 3).zero? && (number % 5).zero?
    'FizzBuzz'
  elsif (number % 3).zero?
    'Fizz'
  elsif (number % 5).zero?
    'Buzz'
  else
    number.to_s
  end
end

def fizz_buzz(start, stop)
  return '' if start > stop

  (start..stop).to_a.map { |number| convert_number number }.join(' ')
end
# END

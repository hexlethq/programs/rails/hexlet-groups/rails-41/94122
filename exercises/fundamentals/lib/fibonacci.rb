# frozen_string_literal: true

# BEGIN
def fibonacci(number)
  return nil if number.negative?

  fibonacci_list = [0, 1]
  start = 3

  start.upto number do
    last_two = fibonacci_list.last 2
    new_fibonacci = last_two.sum
    fibonacci_list << new_fibonacci
  end

  fibonacci_list[number - 1]
end
# END

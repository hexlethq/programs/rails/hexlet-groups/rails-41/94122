# frozen_string_literal: true

require 'open-uri'

class Hacker
  class << self
    def hack(email, password)
      # BEGIN
      sign_up_uri = URI('https://rails-l4-collective-blog.herokuapp.com:443/users/sign_up')

      page = URI.open(sign_up_uri)
      doc = Nokogiri::HTML5(page)

      token = doc.css("input[name='authenticity_token']").attribute('value').value
      cookie = page.meta['set-cookie']

      params = {
        user: {
          email: email,
          password: password,
          password_confirmation: password
        },
        authenticity_token: token
      }

      req = Net::HTTP::Post.new('/users', Cookie: cookie)
      req.set_form_data(params)

      Net::HTTP.start(sign_up_uri.hostname, sign_up_uri.port, use_ssl: true) do |http|
        http.request(req)
      end
      # END
    end
  end
end

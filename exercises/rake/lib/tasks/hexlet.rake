require 'csv'

namespace :hexlet do
  desc 'Import users from csv file'
  task :import_users, [:path] => :environment do |t, args|
    path = args[:path]
    abort 'Error. Data path is required!' unless path
    puts path
    abort "Error. Cant find data file in path #{path}!" unless File.exist?(path)
    _, *users = CSV.read(path)
    users.each do |user|
      first_name, last_name, birthday, email = user
      User.create(
        first_name: first_name,
        last_name: last_name,
        birthday: birthday,
        email: email
      )
      print '.'
    end
  end
end

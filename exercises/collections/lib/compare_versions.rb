# frozen_string_literal: true

# BEGIN
def compare_versions(version_one, version_two)
  major_one, minor_one = (version_one.split '.').map(&:to_i)
  major_two, minor_two = (version_two.split '.').map(&:to_i)

  first_above = (major_one > major_two || minor_one > minor_two)
  second_above = (major_one < major_two || minor_one < minor_two)

  if first_above
    1
  elsif second_above
    -1
  else
    0
  end
end
# END

# frozen_string_literal: true

# rubocop:disable Style/For

def make_censored(sentence, stop_words)
  # BEGIN
  sequence = '$#%!'
  sentence
    .split
    .map { |word| stop_words.include?(word) ? sequence : word }
    .join ' '
  # END
end

# rubocop:enable Style/For

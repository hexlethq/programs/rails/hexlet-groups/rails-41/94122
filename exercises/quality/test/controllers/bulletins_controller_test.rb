require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'open bulletins list page' do
    get bulletins_path

    assert_response :success
    assert_select 'h1', 'Bulletins'
    assert_select 'a', 'Bulletin One'
    assert_select 'a', 'Bulletin Two'
  end

  test 'open published bulletin page' do
    get bulletin_path(bulletins(:bulletin1))

    assert_response :success
    assert_select 'h1', 'Bulletin One'
    assert_select 'p', 'Bulletin body one'
    assert_select 'p', 'Published'
  end

  test 'open unpublished bulletin page' do
    get bulletin_path(bulletins(:bulletin2))

    assert_response :success
    assert_select 'h1', 'Bulletin Two'
    assert_select 'p', 'Bulletin body two'
    assert_select 'p', 'Unpublished'
  end
end

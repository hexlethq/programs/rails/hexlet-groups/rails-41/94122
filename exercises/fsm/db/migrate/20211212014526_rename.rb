class Rename < ActiveRecord::Migration[6.1]
  def change
    rename_column :vacancies, :vacancy_state, :aasm_state
  end
end

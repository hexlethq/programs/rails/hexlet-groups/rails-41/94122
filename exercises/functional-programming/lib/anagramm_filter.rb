# frozen_string_literal: true

# BEGIN
def sort_chars(word)
  word.chars.sort.join
end

def anagramm_filter(original_word, words)
  sorted_word = sort_chars(original_word)
  words.filter { |word| sort_chars(word) == sorted_word }
end
# END

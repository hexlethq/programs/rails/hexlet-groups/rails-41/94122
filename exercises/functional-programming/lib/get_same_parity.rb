# frozen_string_literal: true

# BEGIN
def get_parity(number)
  number.even? ? 'even' : 'odd'
end

def get_same_parity(list)
  return list if list.empty?

  parity_first = get_parity(list.first)
  list.filter { |number| get_parity(number) == parity_first }
end
# END

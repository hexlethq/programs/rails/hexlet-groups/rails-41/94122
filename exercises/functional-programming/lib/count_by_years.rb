# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  users
    .filter { |user| user[:gender] == 'male' }
    .map { |male| male[:birthday][0, 4] }
    .group_by { |year| year }
    .transform_values(&:count)
end
# END

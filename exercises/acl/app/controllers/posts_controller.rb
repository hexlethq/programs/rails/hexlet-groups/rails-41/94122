# frozen_string_literal: true

class PostsController < ApplicationController
  after_action :verify_authorized, except: %i[index show]

  # BEGIN
  def index
    @posts = Post.all
    authorize @posts
  end

  def new
    authorize Post
    @post = current_user.posts.build
  end

  def create
    authorize Post
    @post = current_user.posts.build(post_params)

    if @post.save
      redirect_to post_path @post
    else
      render :new
    end
  end

  def show
    @post = post
    authorize @post
  end

  def edit
    @post = post
    authorize @post
  end

  def update
    @post = post
    authorize @post

    if @post.update(post_params)
      redirect_to post_path @post
    else
      render :edit
    end
  end

  def destroy
    @post = post
    authorize @post

    if @post.destroy
      redirect_to posts_path
    else
      redirect_to post_path @post
    end
  end

  private

  def post
    @post ||= Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end
  # END
end

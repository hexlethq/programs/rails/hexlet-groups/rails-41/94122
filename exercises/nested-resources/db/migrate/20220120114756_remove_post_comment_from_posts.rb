class RemovePostCommentFromPosts < ActiveRecord::Migration[6.1]
  def change
    remove_reference :posts, :post_comment, null: false, foreign_key: true
  end
end

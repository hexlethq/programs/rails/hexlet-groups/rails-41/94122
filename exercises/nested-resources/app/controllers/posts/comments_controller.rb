class Posts::CommentsController < ApplicationController
  def create
    @comment = post.comments.build(comment_params)
    
    if @comment.save
      redirect_to post, notice: 'Success'
    else
      redirect_to post, notice: 'Error'
    end
  end

  def destroy
    @comment = PostComment.find(params[:id])

    @comment.destroy
    redirect_to post, notice: 'Success'
  end

  private

  def post
    @post ||= Post.find(params[:post_id])
  end

  def comment_params
    params.require(:post_comment).permit(:body)
  end
end

# frozen_string_literal: true

require 'test_helper'

class Web::RepositoriesControllerTest < ActionDispatch::IntegrationTest
  # BEGIN
  link = 'https://github.com/octocat/Hello-World'

  test 'should create repo' do
    stub_request(:get, 'https://api.github.com/user/repos')
      .to_return(body: load_fixture('files/response.json'), headers: { 'Content-Type' => 'json' })

    post repositories_path, params: { repository: { link: link } }

    repository = Repository.find_by! link: link
    assert { repository }
  end
  # END
end

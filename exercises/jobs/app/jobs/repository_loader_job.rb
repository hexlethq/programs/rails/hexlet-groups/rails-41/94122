require 'uri'

class RepositoryLoaderJob < ApplicationJob
  queue_as :default

  def perform(id)
    repository = Repository.find(id)
    repository.fetch!

    begin
      client = Octokit::Client.new
      uri = URI.parse(repository.link)
      repo_name = uri.path.split('/').last(2).join('/')
      repo = client.repo(repo_name)

      repository.update(
        link: repo[:html_url],
        owner_name: repo[:owner][:login],
        repo_name: repo[:name],
        description: repo[:description],
        default_branch: repo[:default_branch],
        watchers_count: repo[:watchers_count],
        language: repo[:language],
        repo_created_at: repo[:created_at],
        repo_updated_at: repo[:updated_at],
      )

      repository.resolve!
    rescue StandardError
      repository.reject!
    end
  end
end

# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  extend Forwardable
  include Comparable

  def_delegators :@uri, :scheme, :host
  attr :uri

  def initialize(url)
    @uri = URI(url)
  end

  def query_params
    @uri.query
        .split('&')
        .map { |param| param.split '=' }
        .to_h
        .transform_keys(&:to_sym)
  end

  def query_param(key, default = nil)
    query_params.fetch key, default
  end

  def <=>(other)
    uri <=> other.uri
  end
end
# END

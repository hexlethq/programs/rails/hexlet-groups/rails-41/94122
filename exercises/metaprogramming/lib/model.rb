# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(Methods)
  end

  def initialize(attributes = {})
    @attributes = {}

    self.class.schema.each do |name, option|
      default_attribute = option.fetch :default, nil
      attribute = attributes.fetch name, default_attribute
      @attributes[name] = convert_attribute attribute, option[:type]
    end
  end

  def attributes
    @attributes
  end

  def convert_attribute(attribute, type)
    return attribute if attribute.nil?

    case type
    when :datetime then DateTime.parse attribute
    when :integer  then attribute.to_i
    when :string   then attribute.to_s
    when :boolean  then !attribute == false
    end
  end

  module Methods
    attr_reader :schema

    def attribute(name, options = {})
      @schema ||= {}
      @schema[name] = options

      define_method name do
        @attributes[name]
      end

      define_method "#{name}=" do |attribute|
        @attributes[name] = convert_attribute attribute, options[:type]
      end
    end
  end
end
# END

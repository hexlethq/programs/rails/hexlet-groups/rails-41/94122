# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  completed   :boolean
#  creator     :string
#  description :text
#  name        :string
#  performer   :string
#  status      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  validates :status, inclusion: { in: %w[new closed], message: `%{value} is not a valid value` }
  validates :name, :status, :creator, format: { with: /\A[a-zA-Z ]+\z/, message: 'only allows letters' }
  validates :completed, inclusion: [true, false]
end

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = Task.first
  end

  test 'open tasks list page' do
    get tasks_path

    assert_response :success
    assert_select 'h1', 'Tasks'
  end

  test 'open task page' do
    get task_path(tasks(:task1))

    assert_response :success
    assert_select 'h1', 'Task'
    assert_select 'h2', 'Task 1'
  end

  test 'create new task' do
    post tasks_path, params: {
      task: {
        name: 'New test task',
        description: 'New task text',
        status: 'new',
        creator: 'Dmitry Kabanov',
        performer: 'Dmitry Kabanov',
        completed: false
      }
    }

    assert_response :redirect
    follow_redirect!
    assert_select 'h2', 'New test task'
  end

  test 'not create new task' do
    post tasks_path, params: {
      task: {
        name: '',
        description: 'New task text',
        status: 'new',
        creator: '',
        performer: 'Dmitry Kabanov',
        completed: false
      }
    }

    assert_response :success
    assert_select 'h1', 'New Task'
  end

  test 'update task' do
    patch task_path(@task), params: {
      task: {
        name: 'Updated test task'
      }
    }

    assert_response :redirect
    follow_redirect!
    assert_select 'h2', 'Updated test task'
  end

  test 'delete task' do
    assert_difference('Task.count', -1) do
      delete task_path(@task)
    end
  end
end

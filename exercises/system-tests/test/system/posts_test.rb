# frozen_string_literal: true

require 'application_system_test_case'

# BEGIN
class PostsTest < ApplicationSystemTestCase
  test 'should visiting the index page' do
    visit root_path
  
    assert_selector 'h1', text: 'Home'
  end

  test 'should visiting the posts page' do
    visit posts_path
  
    assert_selector 'h1', text: 'Posts'
  end

  test 'should create new post' do
    visit posts_path

    assert_equal(3, all('tbody tr').count)
  
    click_on 'New Post'

    fill_in 'Title', with: 'New Post title'
    fill_in 'Body', with: 'New Post body'

    click_on 'Create Post'

    assert_text 'Post was successfully created.'

    click_on 'Posts'

    assert_equal(4, all('tbody tr').count)
  end

  test 'shouldn\'t create new post with empty title' do
    visit posts_path
  
    click_on 'New Post'

    click_on 'Create Post'

    assert_text 'Please review the problems below:'
    assert_text 'Title can\'t be blank'

    assert find_field('Title').matches_css?('.is-invalid')
  end

  test 'should edit post' do
    visit posts_path
  
    all('a', text: 'Edit').first.click

    fill_in 'Title', with: 'Updated Post title'
    fill_in 'Body', with: 'Updated Post body'

    click_on 'Update Post'

    assert_text 'Post was successfully updated.'
  end

  test 'should destroy post' do
    visit posts_path

    accept_alert do
      all('a', text: 'Destroy').first.click
    end

    assert_text 'Post was successfully destroyed.'
  end

  test 'should create comment' do
    visit posts_path

    all('a', text: 'Show').first.click

    find_field(id: 'post_comment_body').fill_in with: 'New comment'

    click_on 'Create Comment'

    assert_text 'Comment was successfully created.'
  end

  test 'should delete comment' do
    visit posts_path

    all('a', text: 'Show').first.click

    assert_text 'Two comment'

    accept_alert do
      click_on 'Delete'
    end

    assert page.has_no_text? 'Two comment'
  end

  test 'should update comment' do
    visit posts_path

    all('a', text: 'Show').first.click

    all('a', text: 'Edit').first.click

    find_field(id: 'post_comment_body').fill_in with: 'updated comment'

    click_on 'Update Comment'

    assert_text 'Comment was successfully updated.'
  end
end
# END

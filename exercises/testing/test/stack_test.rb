# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
    @filled_stack = Stack.new %w[ruby php]
  end

  def test_stack_should_return_empty_array
    assert_equal @stack.to_a, []
  end

  def test_stack_should_return_filled_array
    assert_equal @filled_stack.to_a, %w[ruby php]
  end

  def test_stack_should_be_empty
    assert_equal @stack.empty?, true
  end

  def test_stack_should_not_be_empty
    assert_equal @filled_stack.empty?, false
  end

  def test_stack_should_be_zero_size
    assert_equal @stack.size, 0
  end

  def test_stack_should_be_not_zero_size
    assert_equal @filled_stack.size, 2
  end

  def test_should_correctly_push_element_to_empty_stack
    @stack.push! 'ruby'

    assert_equal @stack.empty?, false
    assert_equal @stack.size, 1
    assert_equal @stack.to_a, %w[ruby]
  end

  def test_should_correctly_push_element_to_filled_stack
    @filled_stack.push! 'java'

    assert_equal @filled_stack.size, 3
    assert_equal @filled_stack.to_a, %w[ruby php java]
  end

  def test_should_correctly_pop_element_after_push
    @stack.push! 'ruby'
    @stack.push! 'php'

    assert_equal @stack.size, 2
    assert_equal @stack.to_a, %w[ruby php]

    @stack.pop!

    assert_equal @stack.size, 1
    assert_equal @stack.to_a, %w[ruby]
  end

  def test_should_correctly_pop_from_empty_stack
    @stack.pop!

    assert_equal @stack.size, 0
    assert_equal @stack.to_a, []
  end

  def test_should_correctly_clear_empty_stack
    @stack.clear!

    assert_equal @stack.size, 0
    assert_equal @stack.to_a, []
  end

  def test_should_correctly_clear_stack_after_push
    @stack.push! 'ruby'
    @stack.push! 'php'
    @stack.clear!

    assert_equal @stack.size, 0
    assert_equal @stack.to_a, []
  end

  def test_should_correctly_clear_filled_stack
    @filled_stack.clear!

    assert_equal @filled_stack.size, 0
    assert_equal @filled_stack.to_a, []
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?

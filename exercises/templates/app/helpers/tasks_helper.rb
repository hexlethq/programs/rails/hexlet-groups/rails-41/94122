module TasksHelper
  def format_errors(task, field)
    task.errors.full_messages_for(field)
        .map { |error| content_tag(:div, error) }
        .join
        .html_safe
  end
end
